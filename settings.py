"""
    Settings to be overwriten and used by Questions' API
"""

# Settings
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'DELETE', 'PUT']

XML = False
JSON = True
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-type',
             'If-Match']

# DB name
MONGO_DBNAME = 'eduqc'

# Configure domain
DOMAIN = {
    'questions': {
        'schema': {
            'enunciation': {'type': 'string'},
            'alternatives': {
                'type': 'dict',
                'schema': {
                    'a': {'type': 'string'},
                    'b': {'type': 'string'},
                    'c': {'type': 'string'},
                    'd': {'type': 'string'},
                    'e': {'type': 'string'},
                },
            },
            'answer': {'type': 'string'},
        },
        'item_title': 'question',
    },
    'users': {
        'schema': {
            'username': {'type': 'string'},
            'password': {'type': 'string'}
        }
    }
}
