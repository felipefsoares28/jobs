import Vue from 'vue'

//set axios
import Axios from 'axios'
Vue.prototype.$axios = Axios.create({
  baseURL: "http://192.168.0.19:8000"
})

//set router
import Router from 'vue-router'
import LoginComponent from "./Login.vue"
import AppComponent from "./App.vue"
import HomeComponent from "./Home.vue"
Vue.use(Router)
const router = new Router({
  routes: [
    {
      path: '/',
      component: HomeComponent
    },
    {
      path: '/login',
      component: LoginComponent
    },
    { path: '*', redirect: '/' }
  ]
})

//init app
new Vue({
  // el: '#app',
  router,
  render: h => h(AppComponent)
}).$mount('#app');
